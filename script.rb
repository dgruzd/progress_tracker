class ProgressTracker # Refer to https://gitlab.com/dgruzd/progress_tracker#script for the latest version
  def initialize(total_count, log_delay_seconds: 30, prefix: nil, logger: nil)
    @i = 0; @tc = total_count; @delay = log_delay_seconds.to_i; @mp = prefix; @l = logger; @last_log_at = Time.at(0)
  end

  def iteration(num = 1)
    @i += num; (@last_log_at = Time.now; p = "#{@i}/#{@tc}"; output = "#{@mp}#{p.ljust(@tc.digits.length * 2 + 1)} (#{format "%05.2f", (@i/@tc.to_f) * 100}%)"; (@l ? @l.info(output) : puts(output))) if Time.now - @last_log_at > @delay || @i == @tc
  end
end