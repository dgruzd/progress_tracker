.DEFAULT_GOAL := build

build: update_demo_svg
	./scripts/update_readme.rb
.PHONY:build

record_demo:
	asciinema rec --overwrite --idle-time-limit=5 ./tmp/demo.cast -c ./scripts/demo.rb
.PHONY:record_demo

update_demo_svg: record_demo
	cat ./tmp/demo.cast | svg-term --out images/progress_tracker.svg --window
.PHONY:update_demo_svg