#!/usr/bin/env ruby

REGEXP = /^<!--.+SCRIPT_START.+SCRIPT_END.+-->$/m

readme_content = File.read('README.md')
script = File.read('script.rb')

replace_with = <<-TEMPLATE.strip
<!-- SCRIPT_START -->
```ruby
#{script}
```
<!-- SCRIPT_END -->
TEMPLATE

updated_readme = readme_content.gsub(REGEXP, replace_with)

File.open('README.md', 'w') { |f| f.write(updated_readme) }