#!/usr/bin/env ruby

require_relative '../script.rb'

total_count = 2**16
progress_tracker = ProgressTracker.new(total_count, log_delay_seconds: 1)

1.upto(total_count).each do |i|
  sleep 0.01 if rand > 0.98

  progress_tracker.iteration
end

