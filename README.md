# progress_tracker

## Why this project was created

This is a small script to make the progress of a Ruby snippet more obvious. For example, this might be useful when you execute a change request that iterates over something (let's say `find_each` on a large scope).

The intention that this script will be embedded into the CR to avoid external dependencies. That's why it needs to be compact to make copy-paste into the console less painful.

## Script

<!-- SCRIPT_START -->
```ruby
class ProgressTracker # Refer to https://gitlab.com/dgruzd/progress_tracker#script for the latest version
  def initialize(total_count, log_delay_seconds: 30, prefix: nil, logger: nil)
    @i = 0; @tc = total_count; @delay = log_delay_seconds.to_i; @mp = prefix; @l = logger; @last_log_at = Time.at(0)
  end

  def iteration(num = 1)
    @i += num; (@last_log_at = Time.now; p = "#{@i}/#{@tc}"; output = "#{@mp}#{p.ljust(@tc.digits.length * 2 + 1)} (#{format "%05.2f", (@i/@tc.to_f) * 100}%)"; (@l ? @l.info(output) : puts(output))) if Time.now - @last_log_at > @delay || @i == @tc
  end
end
```
<!-- SCRIPT_END -->


## Usage

```ruby
total_count = Project.count
progress_tracker = ProgressTracker.new(total_count)

Projects.find_each do |project|
  do_work(project)

  progress_tracker.iteration
end
```

## Demonstration

![progress_tracker.svg](./images/progress_tracker.svg)

[Source code of the demonstration script](./scripts/demo.rb)

## Configuration


### Batch processing

By default, the `iteration` method marks one iteration as completed. If you have batch processing, you can pass an argument with the number of completed iterations to it.

```
progress_tracker = ProgressTracker.new(documents.length)
documents.each_slice(1000) do |batch|
  do_work(batch)

  tracker.iteration(batch.count)
end
```

### Log interval

By default, `ProgressTracker` aims to log progress every 30 seconds. This can be changed with the `log_delay_seconds:` argument.
For example, `ProgressTracker.new(total_count, log_delay_seconds: 60)`.

### Message prefix

You can also specify `prefix` to make output more visible. For example, `ProgressTracker.new(total_count, prefix: '> ')`.

### Using custom logger

If you want to use your own logger, you can do that by setting `logger:`. For example, `ProgressTracker.new(total_count, logger: Logger.new($stdout))` to get:

```
I, [2023-11-04T11:54:17.413276 #43349]  INFO -- : 1/65536     (00.00%)
I, [2023-11-04T11:54:18.414708 #43349]  INFO -- : 10017/65536 (15.28%)
I, [2023-11-04T11:54:19.425071 #43349]  INFO -- : 17835/65536 (27.21%)
I, [2023-11-04T11:54:20.433092 #43349]  INFO -- : 26856/65536 (40.98%)
I, [2023-11-04T11:54:21.439033 #43349]  INFO -- : 35693/65536 (54.46%)
I, [2023-11-04T11:54:22.440450 #43349]  INFO -- : 44720/65536 (68.24%)
I, [2023-11-04T11:54:23.450793 #43349]  INFO -- : 53135/65536 (81.08%)
I, [2023-11-04T11:54:24.460231 #43349]  INFO -- : 61669/65536 (94.10%)
I, [2023-11-04T11:54:24.867924 #43349]  INFO -- : 65536/65536 (100.00%)
```
